import base64
import logging
import os
import sys
from datetime import datetime

import boto3
from botocore.exceptions import ClientError
from flask import Flask, redirect, request
from flask_api import status
from flask_cors import CORS

app = Flask(__name__)

# Import configuration from envvars
homepage = os.environ.get("APP_CONF_HOMEPAGE", "https://example.com")
recipient_email = os.environ.get("APP_CONF_RECIPIENT_EMAIL", "youremail@example.com")
sender_email = os.environ.get("APP_CONF_SENDER_EMAIL", "Example <server@example.com>")
redirect_success = os.environ.get("APP_CONF_REDIRECT_SUCCESS", "https://example.com")
redirect_honeypot = os.environ.get("APP_CONF_REDIRECT_HONEYPOT", redirect_success)
aws_region = os.environ.get("APP_CONF_AWS_REGION", "us-west-2")
honeypot_enabled = os.environ.get("APP_CONF_HONEYPOT_ENABLED", "FALSE")
honeypot_enabled = True if honeypot_enabled == "TRUE" else False
honeypot_discard = os.environ.get("APP_CONF_HONEYPOT_DISCARD", "FALSE")
honeypot_discard = True if honeypot_discard == "TRUE" else False
pgp_key = os.environ.get("APP_CONF_PGP_KEY")
if pgp_key:
    pgp_key = base64.b64decode(pgp_key)
debug = os.environ.get("APP_CONF_DEBUG", "FALSE")
debug = True if debug == "TRUE" else False

# Set up logger
logging.basicConfig(level=(logging.DEBUG if debug else logging.INFO))
logger = logging.getLogger("contact-form")

# Attempt to import gnupg and import PGP keys
try:
    import gnupg
except ImportError:
    logger.warning("Disabling PGP support, module gnupg seems to be missing")
    pgp_key = None
else:
    if pgp_key:
        gpg = gnupg.GPG()
        import_result = gpg.import_keys(pgp_key)
        if import_result.count < 1:
            logger.critical("Failed to import PGP key")
            raise ValueError("PGP key data is not valid")
        logger.info(
            f"Using {import_result.count} PGP key(s): {import_result.fingerprints}"
        )
        pgp_fingerprints = import_result.fingerprints


# Allow CORS from Origin: homepage
cors = CORS(app, resources={r"*": {"origins": homepage}})


# Default route in case someone stumbles on to our API
@app.route("/")
@app.route("/submit", methods=["GET"])
def index():
    logger.debug(request.headers)
    return "\n".join(
        [
            "<h2>Are you lost, friend?</h2>",
            "<p>This is an API for email submissions from",
            f'<a href="{homepage}">{homepage}</a>,',
            "try going back there.</p>",
            "",
        ]
    )


# Form submission route
@app.route("/submit", methods=["POST"])
def submit():
    logger.debug(f"Request headers: {request.headers}")
    logger.debug(f"Request form data: {request.form}")

    name = request.form.get("name", "(not provided)")
    replyto = request.form.get("replyto", "(not provided)")
    message = request.form.get("message", "(not provided)")
    honeypot_value = request.form.get("email", "")
    honeypot_triggered = False if honeypot_value == "" else True

    subject = "Contact Form Submission"
    body_text = "\n".join(
        [
            f"The following message was submitted to {homepage}",
            "",
            f"Sender: {name} <{replyto}>",
            f"Date: {datetime.now().replace(microsecond=0)}",
            f"{message}",
            "",
        ]
    )
    charset = "UTF-8"

    if pgp_key:
        data = gpg.encrypt(body_text, pgp_fingerprints, always_trust=True)
        logger.debug(data)
        if not data.ok:
            logger.error(f"PGP encryption failed (reason: {data.status})")
            return "", status.HTTP_500_INTERNAL_SERVER_ERROR
        body_text = str(data)

    # Handle honeypot trigger if needed
    if honeypot_enabled and honeypot_triggered:
        if debug:
            logger.info("Submission triggered honeypot detection")
        if honeypot_discard:
            # refuse to send, just return
            return redirect(redirect_honeypot)
        else:
            # mark the message for filtering
            subject = f"[HONEYPOT FAILURE] {subject}"

    # Create a new SES resource and specify a region.
    client = boto3.client("ses", region_name=aws_region)

    # Try to send the email.
    try:
        client.send_email(
            Destination={"ToAddresses": [recipient_email]},
            Message={
                "Body": {"Text": {"Charset": charset, "Data": body_text}},
                "Subject": {"Charset": charset, "Data": subject},
            },
            Source=sender_email,
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        logger.error(e.response["Error"]["Message"])
        message = "\n".join(
            [
                "<h1>Oops!</h1>",
                "<p>A server error has occurred, please email me at",
                f'<a href="mailto:{recipient_email}">{recipient_email}</a>',
                "instead.</p>",
                f'<p><a href="{homepage}">Return Home</a></p>',
                "",
            ]
        )
        return message, status.HTTP_500_INTERNAL_SERVER_ERROR
    else:
        if honeypot_triggered:
            return redirect(redirect_honeypot)
        return redirect(redirect_success)
