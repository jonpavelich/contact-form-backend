FROM python:3.7-slim AS base
LABEL maintainer="Jon Pavelich <docker@jonpavelich.com>"
ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1

FROM base AS builder
WORKDIR /app
ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1
RUN pip install poetry
ENV VIRTUAL_ENV=/venv
RUN python -m venv ${VIRTUAL_ENV}
ENV PATH="${VIRTUAL_ENV}/bin:${PATH}"
COPY pyproject.toml poetry.lock README.md ./
RUN poetry export -f requirements.txt -E pgp | ${VIRTUAL_ENV}/bin/pip install -r /dev/stdin
COPY contact_form_backend contact_form_backend
RUN poetry build --format wheel
RUN ${VIRTUAL_ENV}/bin/pip install dist/*.whl

FROM base as final
RUN apt-get update && apt-get install -y gnupg
COPY env-template.sh /usr/local/bin/env-template
COPY --from=builder /venv /venv
EXPOSE 8000
CMD ["/venv/bin/gunicorn", "contact_form_backend:app", "--bind=0.0.0.0:8000"]
