# Contact Form Backend
## What is this?
This app lets you use a contact form on your website without revealing your email address. You point your form submission to the app, and it sends you an email with the form contents using Amazon SES. Written in Python using Flask.

## Why?
Static sites are awesome for a lot of reasons, but don't handle form submissions. Tools like [Formspree.io](https://formspree.io) are great, but the free plan doesn't let you hide your email address. This app can be deployed to Heroku for free and handles form submissions from your static site without revealing your email address.

## Installation

### Install using Docker
The application is packaged as a Docker image and published to [Docker Hub](https://hub.docker.com/r/jonpavelich/contact-form-backend).

1. Pull the image from Docker Hub
    ```shell
    $ docker pull jonpavelich/contact-form-backend
    ```

    Alternatively, you can also build the image yourself from source:
    ```shell
    $ git clone https://gitlab.com/jonpavelich/contact-form-backend.git
    $ cd contact-form-backend
    $ docker build -t contact-form-backend .
    ```
2. Export the environment variable template
    ```shell
    $ docker run contact-form-backend env-template > default.env
    ```
3. Edit `default.env` and replace the default values with your own settings
4. Run the image using your environment file, and expose port 8000
    ```shell
    $ docker run --env-file=default.env -p 8000:8000 contact-form-backend
    ```
    You can now submit forms to `http://localhost:8000/submit`
5. For production use, you'll want to set up a reverse proxy (Caddy,   nginx, CloudFlare, etc.) to handle TLS-termination, then set your form to call the submit endpoint (`https://yoursite.example.com/submit`).

### Install from source

1. Ensure you have Python 3.7+ and [poetry](https://python-poetry.org/) installed:
    ```shell
    $ python --version
    Python 3.7.6
    $ poetry --version
    Poetry 1.0.3
    ```
2. Clone the git repository:
    ```shell
    git clone https://gitlab.com/jonpavelich/contact-form-backend.git 
    ```
3. Install the dependencies:
    ```shell
    $ cd contact-form-backend
    $ poetry install
    ```
    Note that if you want to use PGP, you need to have GnuPG installed on your system and you need to include the PGP extras group:
    ```shell
    $ poetry install -E pgp
    ```
4. Set configuration options in your environment, as described below in the [Configuration](#configuration) section 
5. Run the development server:
    ```shell
    $ poetry run contact-form-backend
    ```
    You can now submit forms to `http://localhost:8000/submit`

6. For production use, run the production server instead:
    ```shell
    $ poetry run gunicorn contact_form_backend:app
    ```
    You'll also want to set up a reverse proxy (Caddy, nginx, CloudFlare, etc.) to handle TLS-termination, then set your form to call the submit endpoint (`https://yoursite.example.com/submit`).

## Configuration
### App Config
The following environment variables must be set for the app to work correctly.

| Key                          | Description                          | Example Value                            |
| ---------------------------- | ------------------------------------ | ---------------------------------------- |
| `APP_CONF_HOMEPAGE`          | The URL of the submitting site       | https://yoursite.example.com             |
| `APP_CONF_RECIPIENT_EMAIL`   | The email address to send to         | you@example.com                          |
| `APP_CONF_SENDER_EMAIL`      | The email address to send from       | server@example.com                       |
| `APP_CONF_REDIRECT_SUCCESS`  | The URL to redirect to after sending | https://yoursite.example.com/thanks.html |
| `APP_CONF_AWS_REGION`        | The AWS region configured for SES    | us-west-2 (default)                      |
| `AWS_ACCESS_KEY_ID`          | The AWS/IAM access key ID            | AKIBIYBNOSOKKZYRIX7A                     |
| `AWS_SECRET_ACCESS_KEY`      | The AWS/IAM access key secret        | RdVNq2Zue4znwZYx6+VvBJOSXVdd6KQqmvbq5l68 |

### Form Config
The following form fields are recognized

| Name    | Description                                                   |
| ------- | ------------------------------------------------------------- |
| name    | The user's name                                               |
| replyto | The user's email address (for you to reply to)                |
| message | The message body                                              |
| email   | (Optional) Honeypot field (see [Honeypots](#honeypots) below) |

### Example Form
```html
<form action="https://yoursite.example.com/submit" method="POST">
    <input type="text" name="name" placeholder="Your name">
    <input type="email" name="replyto" placeholder="Your email">
    <textarea name="message" rows="4" placeholder="Your message"></textarea>
    <input type="submit" value="Send">
</form>
```

## PGP Encryption
Optionally, contact form details can be PGP encrypted before being sent. This prevents anyone along the email's path (including Amazon)
from being able to read the message. To configure it, there is another environment variable:

| Key                 | Description                                                                  |
| ------------------- | ---------------------------------------------------------------------------- |
| `APP_CONF_PGP_KEY`  | One or more PGP public keys to encrypt to (ascii format then base64 encoded) |

Base64 is used because the env file cannot contain linebreaks. Run `gpg -a --export YOUR_KEY | base64 -w 0` to export your ASCII-armoured
PGP public key, and encode it as base64 (with no linebreaks). Use the resulting value in your env file. You can specify multiple keys by
concatenating them before base64 encoding.

## Honeypots
Honeypot form fields are supported. You need to set some additional environment variables and add another field to your form.

The `email` field (which is not used - `replyto` contains the user's email address) is a honeypot field. If it contains any value and this setting is enabled, then the submission will be flagged or discarded.

### Environment Variables
| Key                          | Description                                        | Permitted Values                                           |
| ---------------------------- | -------------------------------------------------- | ---------------------------------------------------------- |
| `APP_CONF_REDIRECT_HONEYPOT` | The URL to redirect to when honeypot is triggered  | Any URL (defaults to value of `APP_CONF_REDIRECT_SUCCESS`) |
| `APP_CONF_HONEYPOT_ENABLED`  | Enable or disable honeypot support                 | `TRUE`/`FALSE` (default `FALSE`)                           |
| `APP_CONF_HONEYPOT_DISCARD`  | Discard or flag when honeypot is tripped           | `TRUE`/`FALSE` (default `FALSE`)                           |

When `APP_CONF_HONEYPOT_DISCARD` is `FALSE`, flagged submissions are still sent but the subject is modified to start with `[HONEYPOT FAILURE]`. When it is `TRUE` they are silently discarded. You can use this feature if you are worried about false positives, or in combination with an email filter to send them to spam.

`APP_CONF_REDIRECT_HONEYPOT` allows you to redirect users to a different page notifying them their message was discarded (just in case a real user manages to trip it - bots shouldn't care where you send them after they fill out your form). If you don't set the value, users will be returned to your success page even when the message is discarded.

### Example Form with a Honeypot

**HTML**
```html
<form action="https://yoursite.example.com/submit" method="POST">
    <input type="text" name="name" placeholder="Your name">
    <input type="email" class="honey" name="email" placeholder="Leave this field blank or your message will be discarded" autocomplete="off">
    <input type="email" name="replyto" placeholder="Your email">
    <textarea name="message" rows="4" placeholder="Your message"></textarea>
    <input type="submit" value="Send">
</form>
```

**CSS**
```css
.honey {
    visibility: hidden;
    position: absolute;
    top: -500px;
    left: -500px;
}
```
