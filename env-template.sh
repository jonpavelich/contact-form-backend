#!/bin/bash

config='
## Contact Form Backend - Environment Template
## Usage: save this as "default.env" and add your own values

# Basic configuration (required)
APP_CONF_HOMEPAGE=https://example.com
APP_CONF_RECIPIENT_EMAIL=you@example.com
APP_CONF_SENDER_EMAIL=Contact Form <contact@noreply.example.com>
APP_CONF_REDIRECT_SUCCESS=https://example.com/thanks/
APP_CONF_DEBUG=FALSE

# AWS configuration for SES (required)
APP_CONF_AWS_REGION=us-west-2
AWS_ACCESS_KEY_ID=123456
AWS_SECRET_ACCESS_KEY=123456

# PGP encryption (optional, base64 encoded)
APP_CONF_PGP_KEY=

# Honeypot configuration (optional)
APP_CONF_REDIRECT_HONEYPOT=https://example.com/spam/
APP_CONF_HONEYPOT_ENABLED=FALSE
APP_CONF_HONEYPOT_DISCARD=FALSE
'

echo "$config"
